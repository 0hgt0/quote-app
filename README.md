Создайте базу данных в postgres под названием quotes, запустите скрипт создания структуры БД `src/main/resources/db/sql/init_db.sql`

Для инициализации БД данными разархивируйте json файл и укажите имя json файла в методе `getListOfQuotes` строкой и сделайте GET запрос по адресу `http://localhost:8080/init`

Для получения данных по title сделайте GET запрос, напрмиер `http://localhost:8080/wiki/Бразилия`

Для получения данных с форматирование сделайте GET запрос с параметром ?pretty=true, например `http://localhost:8080/wiki/Бразилия?pretty=true`