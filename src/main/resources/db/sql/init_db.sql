drop category if exists;

drop quote if exists;

drop category_quote if exists;

create table category(
	id serial primary key,
	category varchar unique
);
create table quote(
	id serial primary key,
	wiki varchar,
	auxiliary_text varchar,
	language varchar,
	title varchar,
	timestamp timestamp,
	create_timestamp timestamp
);
create table category_quote(
	id serial primary key,
	quote_id integer not null ,
	category_id integer not null,
	foreign key (quote_id) references quote(id),
	foreign key (category_id) references category(id)
)