package service

import cats.effect.IO
import config.Config
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.deriveDecoder
import io.circe.parser._
import io.circe.syntax._
import model.{Quote, QuoteNotFound}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.`Content-Type`
import org.http4s.{HttpRoutes, MediaType, Response}
import repository.QuoteRepository

import scala.io.Source

class QuoteService(repository: QuoteRepository) extends Http4sDsl[IO] {

  object PrettyQueryParam extends OptionalQueryParamDecoderMatcher[String]("pretty")

  val routes = HttpRoutes.of[IO] {

      case GET -> Root / "wiki" / title :? PrettyQueryParam(param) =>
      for {
        getResult <- repository.getQuote(title.toLowerCase())
        response <- getQuote(getResult, param)
      } yield response
    case GET -> Root / "init" =>
      val listOfAllQuotes = getListOfQuotes("json/test.json")
      val listOfCategories = for {
        quote <- listOfAllQuotes
        category <- quote.category
      } yield category
      listOfCategories.toSet.foreach(s => repository.insertCategory(s))
      listOfAllQuotes.foreach(q => repository.insertQuote(q))
      Ok("Ok")
  }

  private def getQuote(result: Either[QuoteNotFound.type, Quote], param: Option[String]): IO[Response[IO]] = {
    result match {
      case Left(QuoteNotFound) => NotFound()
      case Right(value) => param match {
        case Some(str) if str == "true" => Ok(value.asJson.toString, `Content-Type` (MediaType.application.json))
        case None => Ok(value.asJson.noSpaces, `Content-Type` (MediaType.application.json))
      }
    }
  }

  private def getListOfQuotes(pathToJsonFile: String): List[Quote] = {
    val list = collection.mutable.ListBuffer.empty[Quote]
    val source = Source.fromFile(pathToJsonFile)
    for (line <- source.getLines()) {
      val d = decode[Quote](line)
      d.map(c => list.+=(c))
    }
    list.toList
  }

  implicit val quoteDecoder: Decoder[Quote] = deriveDecoder[Quote]
  implicit val quoteEncoder: Encoder[Quote] = Encoder.forProduct7("wiki", "auxiliary_text", "language", "title",
    "category", "timestamp", "create_timestamp")(q =>
    (q.wiki, q.auxiliary_text, q.language, q.title, q.category, q.timestamp.getEpochSecond, q.create_timestamp.getEpochSecond))

}
