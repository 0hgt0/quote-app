import java.time.Instant

package object model {

  case class Quote(
                   wiki: String,
                   auxiliary_text: List[String],
                   language: String,
                   title: String,
                   category: List[String],
                   timestamp: Instant,
                   create_timestamp: Instant
                  )

  case object QuoteNotFound

}
