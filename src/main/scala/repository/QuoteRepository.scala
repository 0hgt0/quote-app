package repository

import java.time.Instant

import cats.effect.IO
import doobie._
import doobie.implicits._
import doobie.util.Read
import doobie.util.transactor.Transactor
import model.{Quote, QuoteNotFound}

class QuoteRepository(transactor: Transactor[IO]) {
  implicit val meta: Meta[List[String]] = Meta[String].imap(_.split(",").toList)(_.mkString(","))

  implicit val read: Read[Quote] = Read[(String, List[String], String, String, List[String], Instant, Instant)].map {
    case (wiki: String, auxiliary_text: List[String], language: String, title: String, category: List[String], timestamp: Instant, create_timestamp: Instant) => Quote(wiki, auxiliary_text, language, title, category, timestamp, create_timestamp)
  }
  implicit val write: Write[Quote] = Write[(String, List[String], String, String, List[String], Instant, Instant)].contramap {
    q => (q.wiki, q.auxiliary_text, q.language, q.title, q.category, q.timestamp, q.create_timestamp)
  }


  def getQuote(title: String): IO[Either[QuoteNotFound.type, Quote]] = {
    val s = sql"""SELECT wiki, auxiliary_text, language, title, string_agg(c.category, ', ') as category, timestamp, create_timestamp FROM public.quote as q, public.category as c , public.category_quote as qc WHERE lower(title) = $title AND q.id = qc.quote_id AND c.id = qc.category_id GROUP BY 1,2,3,4,6,7""".query[Quote].option.transact(transactor).map {
      case Some(quote) => Right(quote)
      case None => Left(QuoteNotFound)
    }
    s
  }

  def insertCategory(category: String): IO[Long] = {
    val id = sql"insert into category(category) values ($category)".update.withUniqueGeneratedKeys[Long]("id").transact(transactor)
    id.unsafeRunSync()
    id
  }

  def insertQuote(quote: Quote): IO[Long] = {
    val id =
      sql"""insert  into quote(wiki, auxiliary_text, language, title, timestamp, create_timestamp) values (${quote.wiki}, ${quote.auxiliary_text},${quote.language},${quote.title},${quote.timestamp}, ${quote.create_timestamp})""".update.withUniqueGeneratedKeys[Long]("id").transact(transactor)

    val idL = id.unsafeRunSync()
    val listOfIdsOfCategories = for {
      cat <- quote.category
      id <- sql"select id from category where category = $cat".query[Long].stream.take(1).compile.toList.transact(transactor).unsafeRunSync()
    } yield id
    listOfIdsOfCategories.foreach(id => {
      sql"insert into category_quote(quote_id, category_id) values ($idL, $id)".update.withUniqueGeneratedKeys[Long]("id").transact(transactor).unsafeRunSync()
    })
    id
  }

}
